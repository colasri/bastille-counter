#!/usr/bin/env python

from time import sleep
import requests

def main():
    '''Download webcam images corresponding to
    https://bastille-grenoble.fr/webcam-bastille-telepherique-grenoble,
    from the OVH server that hosts them.
    '''
    baseUrl = 'http://storage.gra.cloud.ovh.net/v1/AUTH_0e308f8996f940d38153db4d0e7d7e81/static/GrenobleBastille'
    outPath = './img'
    # Last sunny Bastille's day to date
    year = 2020
    month = 7
    day = 14

    date = '{}/{:02d}/{:02d}'.format(year, month, day)
    for hour in range(23):
        hourSteps = 1
        if hour >= 7 and hour < 18:
            hourSteps = 3 # There are 3 pics per hour during daytime, just one at night
        minuteStep = 60//hourSteps
        for step in range(hourSteps):
            for dminute in range(1, 5): # Pictures are not always timestamped exactly at the expected round value, try a few
                minute = step*minuteStep+dminute
                url = '{}/{}/{:02d}-{:02d}.jpg'.format(baseUrl, date, hour, minute)
                print(url)
                output = '{}/{}-{:02d}-{:02d}-{:02d}-{:02d}.jpg'.format(outPath, year, month, day, hour, minute)
                print(output)
                sleep(0.2) # Be nice to OVH servers, we don't want them to burn again...
                r = requests.get(url)
                print('HTTP request status code: {:d}'.format(r.status_code))
                if r.status_code == 200:
                    with open(output, 'wb') as f:
                        f.write(r.content)
                    print('Downloaded image '+output)
                    break
                else:
                    print('Image '+output+' not found')

if __name__ == "__main__":
    main()
