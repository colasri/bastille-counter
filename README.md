[![Codacy Badge](https://app.codacy.com/project/badge/Grade/3fd4dcd3c2ed46b1b6fd1465b8c5f7c2)](https://www.codacy.com/gl/colasri/bastille-counter/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=colasri/bastille-counter&amp;utm_campaign=Badge_Grade)

# bastille-counter

Count number of people on Grenoble's "Terrasse des Géologues" on the Bastille
(aka a simple script to illustrate how to tag and count objects in a picture).

## Environment setup

```bash
conda create -n pybas python=3
conda activate pybas
pip install opencv-python tensorflow matplotlib jupyter pandas cvlib PyQt5
```

## Download webcam pictures

A great panoramic view from the Bastille, Grenoble, France can be found on the
[Téléphérique's website](https://bastille-grenoble.fr/webcam-bastille-telepherique-grenoble/).
Archival pictures are stored on OVS' servers, like [this](http://storage.gra.cloud.ovh.net/v1/AUTH_0e308f8996f940d38153db4d0e7d7e81/static/GrenobleBastille/2020/07/14/16-21.jpg):

![example panorama](sample/2020-07-14-14-21.jpg)

The `./get_files.py` script downloads all the pictures from the last Bastille's day with great weather, 2020-07-14.

## Analyse pictures

The `./counter.py` script first crops the input image on the terrace region on the right, then tags the people (among other things) to count them.
Two methods were used for the tagging:

-   First, using [cvlib](https://github.com/arunponnusamy/cvlib), a minimalistic library whose tagging method is based on [YOLOv4](https://github.com/AlexeyAB/darknet) model trained on [COCO dataset](http://cocodataset.org/) capable of detecting 80 common objects.
-   Then using [DETR](https://huggingface.co/facebook/detr-resnet-50) (Facebook's DEtection TRansformer) with pytorch, also trained on COCO 2017. The confidence threshold was slightly tuned to improve detection (type II errors significantly reduced while maintaining low type I error).

Here are example tagged images corresponding to the panorama picture above, with YOLO and DETR:

![example tag](sample/2020-07-14-14-21_tagged_cvlib.jpg)
![example tag](sample/2020-07-14-14-21_tagged_detr.jpg)

Somewhat hit-and-miss, but overall not too bad tagging.
One can derive e.g. the affluence as a function of time.
The higher detection performance of DETR is large here, though the threshold was not tuned in the case of YOLO, which prevents a fair comparison.

![timeline](sample/timeline.png)
