#!/usr/bin/env python

import cvlib as cv
from cvlib.object_detection import draw_bbox
import cv2
import numpy as np
import pandas as pd
import glob
from transformers import DetrFeatureExtractor, DetrForObjectDetection
import sys
import torch

import matplotlib
matplotlib.use("Qt5Agg")

import matplotlib.pyplot as plt
from matplotlib.dates import DateFormatter

def plot_results_detr(image, prob, boxes, labels, outfile, display=False):
    plt.figure(figsize=(16,10))
    plt.imshow(image)
    ax = plt.gca()
    for p, (xmin, ymin, xmax, ymax), l in zip(prob, boxes.tolist(), labels):
        cl = p.argmax()
        ax.add_patch(plt.Rectangle((xmin, ymin), xmax - xmin, ymax - ymin,
                                   fill=False, color='green', linewidth=3))
        text = f'{l}: {p[cl]:0.2f}'
        # text = f'{model.config.id2label[cl.item()]}: {p[cl]:0.2f}'
        ax.text(xmin, ymin, text, fontsize=7,
                bbox=dict(facecolor='yellow', alpha=0.5))
    plt.axis('off')
    plt.savefig(outfile)
    if display:
        plt.show()

def count(inputName, cvmodel='cvlib'):
    # read input image
    image = cv2.imread(inputName)
    print(image.shape)
    image = image[1100:, 8050:, :]
    print(image.shape)

    outputName = './img/tag/'+ inputName.split('/')[-1].split('.')[0] + f'_tagged_{cvmodel}.jpg'
    print(inputName,' ==> ', outputName)

    # apply object detection
    if cvmodel == 'cvlib':
        bbox, label, conf = cv.detect_common_objects(image)
        print(bbox, label, conf)

        # draw bounding box over detected objects
        out = draw_bbox(image, bbox, label, conf)
        cv2.imshow("object_detection", out)

        # save output
        cv2.imwrite(outputName, out)

        counts = pd.Series(label, dtype='str').value_counts()

    elif cvmodel == 'detr':
        feature_extractor = DetrFeatureExtractor.from_pretrained('facebook/detr-resnet-50')
        model = DetrForObjectDetection.from_pretrained('facebook/detr-resnet-50')

        inputs = feature_extractor(images=image, return_tensors="pt")
        outputs = model(**inputs)

        # keep only predictions of queries with enough confidence (excluding no-object class)
        keep_threshold = 0.8
        probas = outputs.logits.softmax(-1)[0, :, :-1]
        # keep only the 'person' class
        labels = np.array([model.config.id2label[p.argmax().item()] for p in probas])
        keep = (probas.max(-1).values > keep_threshold) & (labels == 'person')
        keep = np.array(keep, dtype='bool')

        # Count all object passing threshold, 'person' or not
        counts = pd.Series([l for l,p in zip(labels, probas.max(-1).values) if p>keep_threshold], dtype='str').value_counts()

        # rescale bounding boxes
        target_sizes = torch.tensor([image.shape[0], image.shape[1]]).unsqueeze(0)
        print(f'Target sizes {target_sizes}')
        postprocessed_outputs = feature_extractor.post_process(outputs, target_sizes)
        bboxes_scaled = postprocessed_outputs[0]['boxes']

        plot_results_detr(image[:,:,[2,1,0]], probas[keep], bboxes_scaled[keep], labels[keep], outputName)
    else:
        print(f'Unknown model "{model}", should de "cvlib" or "detr".')
        sys.exit(1)

    # release resources
    cv2.destroyAllWindows()

    print(counts)
    return counts.get('person', default=0)


if __name__ == "__main__":
    data = []
    for inputName in sorted(glob.glob('./img/*.jpg')):
        print(inputName)
        c = count(inputName)
        c2 = count(inputName, cvmodel='detr')
        dts = inputName.split('/')[-1].split('.')[0]
        dt = pd.to_datetime(dts, format='%Y-%m-%d-%H-%M')
        print(dt, c)
        data.append([dt, c, c2])

    df = pd.DataFrame(data, columns=['time', 'people (YOLOv4)', 'people (DETR)'])
    print(df)

    fig, ax = plt.subplots(figsize=(15,8))
    for model in ['YOLOv4', 'DETR']:
        ax.scatter(df['time'], df[f'people ({model})'], label=model)
    myFmt = DateFormatter("%Y-%m-%d %Hh00")
    ax.xaxis.set_major_formatter(myFmt)
    fig.autofmt_xdate() # Rotate date labels automatically
    ax.set_ylabel('Number of people')
    ax.set_title('Terrasse des Géologues')
    ax.legend()
    fig.savefig('timeline.png', facecolor='white')
